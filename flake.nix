{
  description = "A collection of lists of mimetypes for different purposes.";
  outputs =
    { self, ... }:
    let
      pipe = builtins.foldl' (x: f: f x);
    in
    {
      lib.mimetypes = pipe ./src [
        __readDir
        __attrNames
        (map (
          name: {
            name = __head (__split "\\." name);
            value = __fromJSON (__readFile ./src/${name});
          }
        ))
        __listToAttrs
      ];
    };
}
