# MIME Types

This is my collection of useful mimetypes sorted into lists exposed as a nix flake.

To use this in NixOS to make mpv the default video player and neovim the default text editor, you could for example do:

```nix
{
    config,
    lib,
    ...
}: {
    xdg.mime.defaultApplications = 
        (lib.genAttrs lib.mimetypes.video (_: ["mpv.desktop"]))
        // (lib.genAttrs lib.mimetypes.text (_: ["nvim.desktop"]));
    xdg.mime.addedAssociations = config.xdg.mime.defaultApplications;
}
```
